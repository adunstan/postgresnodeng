#!/bin/perl

use strict;
use warnings;

# PostgresNode (via TestLib) hijacks stdout,
# so make a dup before it gets a chance
use vars qw($out);
BEGIN
{
    open ($out, ">&STDOUT");
	system("rm -rf tmp_check");
}

use lib '/home/andrew/pgl/pg_head/src/test/perl';
use lib '/home/andrew/pgl/postgresnodeng';

use PostgresNode;

$ENV{PG_REGRESS} = '/bin/true'; # stupid but necessary


my %node_map = (

	 v_devel => {
		path => '/home/andrew/bf/root/saves.crake/HEAD',
		streaming => 'logical',
		backup => []
		  },

	v_14 => {
		path => '/home/andrew/bf/root/saves.crake/REL_14_STABLE',
		streaming => 'logical',
		backup => []
		  },
	v_13 => {
		path => '/home/andrew/bf/root/saves.crake/REL_13_STABLE',
		streaming => 'logical',
		backup => []
		  },
	v_12 => {
		path => '/home/andrew/bf/root/saves.crake/REL_12_STABLE',
		streaming => 'logical',
		backup => []
		  },
	v_11 => {
		path => '/home/andrew/bf/root/saves.crake/REL_11_STABLE',
		streaming => 'logical',
		backup => []
		  },
	v_10 => {
		path => '/home/andrew/bf/root/saves.crake/REL_10_STABLE',
		streaming => 'logical',
		backup => []
		  },
	
	v96  => {
		path => '/home/andrew/bf/root/saves.crake/REL9_6_STABLE',
		streaming => 'logical',
		backup => ['-X', 'stream']
		  },
	v95  => {
		path => '/home/andrew/bf/root/saves.crake/REL9_5_STABLE',
		streaming => 'logical',
		backup => ['-X', 'stream']
		  },		
	v94  => {
		path => '/home/andrew/bf/root/upgrade.crake/REL9_4_STABLE/inst',
		streaming => 'logical',
		backup => ['-X', 'stream']
		  },
	v93  => {
		path => '/home/andrew/bf/root/upgrade.crake/REL9_3_STABLE/inst',
		streaming => 'physical',
		backup => ['-X', 'stream']
		  },
	v92  => {
		path => '/home/andrew/bf/root/upgrade.crake/REL9_2_STABLE/inst',
		streaming => 'physical',
		backup => ['-X', 'stream']
		  },
	v91 => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/9.1',
		streaming => 'physical',
		backup => ['-x']
		  },		
	v90 => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/9.0',
		streaming => 'physical',
		backup => 0
		  },
	v84  => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/8.4',
		streaming => 0,
		backup => 0
		  },
	v83  => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/8.3',
		streaming => 0,
		backup => 0
		  },
	v82  => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/8.2',
		streaming => 0,
		backup => 0
		  },
	v81  => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/8.1',
		streaming => 0,
		backup => 0
		  },
	v80  => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/8.0',
		streaming => 0,
		backup => 0
		  },
	v74 => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/7.4',
		streaming => 0,
		backup => 0
		  },
	v73 => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/7.3',
		streaming => 0,
		backup => 0
		  },
	v72 => {
		path => '/home/andrew/pgl/pgold/opt/pgsql/7.2',
		streaming => 0,
		backup => 0
		  },
   );

my @nodes;

my $arg = shift;

my @tnodes = reverse sort keys %node_map;

@tnodes = split(/\s+/, $arg) if defined $arg;

foreach my $name (@tnodes)
{
	die "No map for $name" unless exists $node_map{$name};
	my $node = PostgresNode->new($name, install_path => $node_map{$name}->{path});
	push @nodes, $node;
}

foreach my $node (@nodes)
{
	print $out $node->info();
	my $name = $node->name;
	$node->init (allows_streaming => $node_map{$name}->{streaming});
	$node->start;
	# require this locution for versions less than 8.3 where psql learned to use connstrs
	my $host = $node->host;
	my $port = $node->port;
	my %options = ( host => $node->host, port => $node->port, );
	my $version = $node->safe_psql('template1', 'select version()', %options);
	print $out "version reported by node: $version\n";
	if ($node_map{$name}->{backup})
	{
		$node->backup('mybackup', backup_options => $node_map{$name}->{backup});
	}

	if ($node_map{$name}->{backup})
	{
		my $newname = "$name" . "_copy";
		my $newnode = PostgresNode->new($newname, install_path => $node_map{$name}->{path});

		$newnode->init_from_backup($node, 'mybackup', has_streaming => 1);
		$newnode->start;
		$node->wait_for_catchup($newnode);
		my $version = $newnode->safe_psql('template1', 'select version()', %options);
		print $out "version reported by new node: $version\n";
		$newnode->stop;
	}

	$node->stop();


}
